output "vpc_id" {
  value = aws_vpc.vpc.id
}
output "public_subnets_id_a" {
  value = element(aws_subnet.public_subnet.*.id, 0)
}
output "public_subnets_id_b" {
  value = element(aws_subnet.public_subnet.*.id, 1)
}

output "private_subnets_id_a" {
  value = element(aws_subnet.private_subnet.*.id, 0)
}
output "private_subnets_id_b" {
  value = element(aws_subnet.private_subnet.*.id, 1)
}
