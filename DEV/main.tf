terraform {
    required_providers {
        aws = {
            source  = "hashicorp/aws"
            version = ">= 3.27"
        }
    }
}

provider "aws" {
    profile = "default"
    region  = "ap-southeast-1"
}

locals {
  production_availability_zones = ["${var.aws_region}a", "${var.aws_region}b"]
}

module "vpc" {
    source               = "../module/VPC"
    environment          = var.environment
    vpc_cidr             = var.vpc_cidr
    public_subnets_cidr  = var.public_subnets_cidr
    private_subnets_cidr = var.private_subnets_cidr
    availability_zones   = local.production_availability_zones
}

module "EC2" {
    source               = "../module/EC2"
    vpc_id               = module.vpc.vpc_id
    subnet_id            = module.vpc.public_subnets_id_a
    count_number         = 2
}
