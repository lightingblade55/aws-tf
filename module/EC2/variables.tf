variable "project_name" {
  default     = ["ec2-1", "ec2-2"]
  type        = list
  description = "project name injected into task"
}

variable "ami" {
  default     = "ami-0126086c4e272d3c9" #linux amazon
  type        = string
  description = "ami id for creating Ec2 instance"
}

variable "instance_type" {
  type        = string
  default     = "t2.2xlarge"
  description = "instance type for creating EC2 instance"
}

variable "vpc_id" {
  type        = string
  description = "VPC id for security group"
}

variable "subnet_id" {
  type        = string
  description = "subnet id for EC2 instance to be included"
}

variable "count_number" {}