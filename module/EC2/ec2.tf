locals {
  ami           = var.ami
  instance_type = var.instance_type
  name          = var.project_name
  vpc_id        = var.vpc_id
  subnet_id     = var.subnet_id
}

resource "aws_instance" "vdi_test" {
  count         = var.count_number
  ami           = local.ami
  instance_type = local.instance_type
  subnet_id     = local.subnet_id
  key_name      = "vdi_test"
  user_data     = file("../module/EC2/user_data_backup.sh")
  ebs_block_device {
    volume_size = 150
    volume_type = "gp3"
    device_name = "/dev/xvda"
  }
  tags = {
    Name = "vdi_test-${count.index}"
  }
  vpc_security_group_ids = [aws_security_group.allow_ssh.id]
}

resource "aws_security_group" "allow_ssh" {
  name        = "allow-ssh-ec2"
  description = "Allow SSH inbound traffic"
  vpc_id      = local.vpc_id

  ingress { // inbound
    description = "Allow SSH"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress { // inbound
    description = "Allow range port 8090-8099"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress { // inbound
    description = "Open port 3000"
    from_port   = 3000
    to_port     = 3000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
   ingress { // inbound
    description = "Allow SSH"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress { // outbound
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  tags = {
    Name = "allow_ssh"
  }
}
